using namespace std;
#include <iostream>
#include <fstream>
#include <ctime>
#include <iomanip>


struct Database {
    char name;
    int price;
};
struct Reference {
    int x;
    int y;
};

int main() {
    const int n = 8; //rozmiar tablicy;
    int cheapest = INT_MAX;
    int pricest = -1;
    Reference refCheap{-1,-1};
    Reference refPricest{-1,-1};
    Database A[n][n];

//    srand(8);
    srand(time(0));

    for (int i = 0; i <n; ++i) {
        for (int j = 0; j < n; ++j) {
            //wylosuj
            A[i][j].price = (rand() % 98) + 1;
            A[i][j].name = 'a' + rand() % 26;
            //podmień litere
            if (
                    i==j || // glowna przekątna
                    i + j + 1 == n || // 2ga przekatna
                    i == 0 || // boki
                    j == 0 ||
                    i == n-1 ||
                    j == n -1
                ) {
                A[i][j].name = toupper(A[i][j].name);
            }
            // najtańszy
            if (i + 2 <= j && j >= 2) {
                cheapest = min(cheapest, A[i][j].price);
                if ((A[i][j].price == cheapest) && (refCheap.x > -1)) {
                    refCheap.x = i;
                    refCheap.y = j;
                }
            }
            //najdroższy
            if (j >= n - 3) {
                pricest = max(pricest, A[i][j].price);
                if ((A[i][j].price == pricest) && (refPricest.x > -1)){
                    refPricest.x = i;
                    refPricest.y = j;
                }
            }

            cout << A[i][j].name << (isupper(A[i][j].name) ? " " : "") << A[i][j].price << '\t';
        }
        cout << endl;
    }
    cout << endl;

    //zamień
    A[refPricest.x][refPricest.y].price = cheapest;
    A[refCheap.x][refCheap.y].price = pricest;

    //wydrukuj dane i zapisz do pliku
    string filename = "ceny.txt";
    ofstream file(filename.c_str());
    for (int i = 0; i <n; ++i) {
        for (int j = 0; j < n; ++j) {
            bool isUpper = isupper(A[i][j].name);
            cout << A[i][j].name << (isUpper ? " " : "") << A[i][j].price << '\t';
            file << A[i][j].name << (isUpper ? " " : "") << A[i][j].price << '\t';
        }
        cout << endl;
        file << endl;
    }
    cout << endl;
    file.close();

    return 0;
}
