# Zadanie 6 - z rekordami (0. 5 pkt)

1. Do tablicy towarów A\[n\]\[n\] (n - stała) wylosować rekordy o polach (nazwa towaru - typu char, cena - typu int) w taki sposób, aby:

* * nazwy towarów były dużymi literami na obwodzie i na obu przekątnych tablicy, zaś
małymi literami w pozostałym obszarze tablicy.
* * ceny były jedno- lub dwucyfrowe.

2. Tablicę wydrukować wierszami w postaci kolejnych rekordów, np. a10 R 7 w93…
3. Zamienić miejscami najtańszy towar leżący nad drugą przekątną tablicy z najdroższym towarem leżącym w trzech ostatnich wierszach tej tablicy.
4. Ponownie wydrukować wierszami tablicę oraz zapisać ją wierszami do pliku ceny.txt, w formacie jak w p.2.

> Wskazówka: Najpierw należy zdefiniować strukturę rekordów.

> Przypomnienie: losowanie liczb i znaków jest omówione na końcu lekcji 3.3.